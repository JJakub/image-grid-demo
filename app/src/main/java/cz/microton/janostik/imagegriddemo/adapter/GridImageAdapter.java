package cz.microton.janostik.imagegriddemo.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import cz.microton.janostik.imagegriddemo.R;
import cz.microton.janostik.imagegriddemo.view.SquaredImageView;

/**
 * Simple adapter for gridview
 *
 * TODO: Add checkable interface for multicheck functinality
 * TODO: Add favourable interface for favourites functionality
 * TODO: Right now adapter only uses simple SquaredImageView custom layout. For showing favourites/selected items custom layout will need to be added
 *
 * Created by jakub on 27/07/15.
 */
public class GridImageAdapter extends BaseAdapter {

    private List<String> imagePaths;
    private Context context;
    private static LayoutInflater inflater = null;

    public GridImageAdapter(Activity activity, List<String> imagePaths) {

        this.imagePaths = imagePaths;
        this.context = activity;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return imagePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SquaredImageView view = (SquaredImageView) convertView;
        if (view == null) {
            view = new SquaredImageView(context);
        }
        String url = (String)getItem(position);

        // Convert 100 dp to px to size image
        // TODO: use resource file for value 100
        int sizeInPx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources().getDisplayMetrics());
        Picasso.with(context).load(new File(url))
                .resize(sizeInPx, sizeInPx)
                .centerCrop()
                .into(view);

        return view;
    }
}
