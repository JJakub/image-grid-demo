package cz.microton.janostik.imagegriddemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cz.microton.janostik.imagegriddemo.adapter.GridImageAdapter;
import cz.microton.janostik.imagegriddemo.util.DocumentExtractor;
import cz.microton.janostik.imagegriddemo.util.DocumentCreator;

/**
 * Starting activity containing grid layout with image thumbnails
 *
 * Uses ButterKnife for view binding
 * Uses Picasso for image viewing
 *
 * TODO: Multiselect for gridview. Sharing prepared in method dispatchShareMultipleImagesIntent(urls)
 * TODO: Custom transition because of support for api 15
 * TODO: Use appcompat toolbar instead of default ActionBar
 */
public class GridActivity extends AppCompatActivity {

    private static final String TAG = GridActivity.class.getName();
    private static final int REQUEST_TAKE_PHOTO = 1;

    @Bind(R.id.grid) GridView gridView;

    private List<String> imagePaths;
    private GridImageAdapter gridImageAdapter;
    private List<String> sharedImages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        // Bind views based on butterknife annotations
        ButterKnife.bind(this);

        imagePaths = DocumentExtractor.extractImages(this);
        if(imagePaths.isEmpty()) {
            Toast.makeText(this, R.string.no_images_error, Toast.LENGTH_SHORT).show();
        } else {
            gridImageAdapter = new GridImageAdapter(this, imagePaths);
            gridView.setAdapter(gridImageAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent detailIntent = new Intent(getApplicationContext(), DetailActivity.class);

                    // TODO: This requires api > 16. Handle custom transition for api 15.
//                    ImageView imageView = (ImageView) view.findViewById(R.id.image);
//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(GridActivity.this, imageView, "profile");
                    detailIntent.putExtra(DetailActivity.IMAGE_BUNDLE_KEY, imagePaths.get(position));

//                    startActivity(detailIntent, options.toBundle());
                    startActivityForResult(detailIntent, -1); // -1 do not return request code

                }
            });
            gridView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Intent for taking image using camera
        if (id == R.id.action_camera) {
            dispatchTakePictureIntent();
            return true;
        }

        // TODO: For now share button commented out in menu_grid.xml
        if (id == R.id.action_share) {
            dispatchShareMultipleImagesIntent(sharedImages);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String lastImagePath = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If correct result. Add path. Adapter will take notice of new path and will add new image.
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK && lastImagePath != null) {
//            Bundle extras = data.getExtras();
//            imagePaths.add(extras.getString(DetailActivity.IMAGE_BUNDLE_KEY));
            imagePaths.add(lastImagePath);
        }
    }

    /**
     * Sends intent for camera. Returns in onActivityResult with requestCode REQUEST_TAKE_PHOTO
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = DocumentCreator.createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, ex.getLocalizedMessage());

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                lastImagePath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                takePictureIntent.putExtra(DetailActivity.IMAGE_BUNDLE_KEY, photoFile.getAbsolutePath());
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * Share multiple items. Selectable interface needs to be implemented before this method can be used.
     *
     * @param sharedImagePaths
     */
    private void dispatchShareMultipleImagesIntent(List<String> sharedImagePaths) {
        Intent share = new Intent(Intent.ACTION_SEND_MULTIPLE);
        share.putExtra(Intent.EXTRA_SUBJECT, "Sharing images...");
        share.setType("image/jpeg");

        ArrayList<Uri> files = new ArrayList<Uri>();
        for(String path : sharedImagePaths) {
            File file = new File(path);
            Uri uri = Uri.fromFile(file);
            files.add(uri);
        }
        share.putExtra(Intent.EXTRA_STREAM, files);
        startActivity(Intent.createChooser(share, "Share Images"));
    }
}
