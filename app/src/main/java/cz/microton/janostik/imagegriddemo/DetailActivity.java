package cz.microton.janostik.imagegriddemo;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Detail activity containing single image view
 */
public class DetailActivity extends AppCompatActivity {

    public static final String IMAGE_BUNDLE_KEY = "image";

    @Bind(R.id.image) ImageView imageView;

    private String imagePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Bind views based on butterknife annotations
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        imagePath = bundle.getString(IMAGE_BUNDLE_KEY);

        Picasso.with(this).load(new File(imagePath))
                .into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share && imagePath != null) {
            dispatchShareImageIntent(imagePath);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void dispatchShareImageIntent(String imagePath) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + imagePath));
        startActivity(Intent.createChooser(share, "Share Image"));
    }
}
