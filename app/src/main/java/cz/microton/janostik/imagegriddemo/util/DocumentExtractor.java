package cz.microton.janostik.imagegriddemo.util;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Util class for extracting paths of documents on device
 *
 * Created by jakub on 27/07/15.
 */
public class DocumentExtractor {

    public static List<String> extractImages(Activity activity) {

        List<String> images = new ArrayList<>();

        Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
        int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        String PathOfImage = null;
        while (cursor.moveToNext()) {
            PathOfImage = cursor.getString(column_index_data);
            images.add(PathOfImage);
        }

        return images;
    }
}
